#!/usr/bin/env bash
RUNTIME_START=$(date +%s)

DIR="$(dirname ${0})/../"
cd ${DIR}

./bin/subscripts/check_env.sh
if [[ $? -ne 0 ]]; then exit 1; fi

if [[ ! -f ./bin/external/yq ]]; then
    curl -sL https://github.com/mikefarah/yq/releases/download/2.4.0/yq_linux_amd64 -o ./bin/external/yq
    chmod +x ./bin/external/yq
fi

TMP_FILE=$(tempfile)
./bin/subscripts/docker-compose.sh config > ${TMP_FILE}

filter() {
    CONFIG_KEY=${1}
    SERVICE_NAME=${2}

    if [[ ! $(grep -r ${CONFIG_KEY} .env | cut -d= -f2) == "true" ]]; then
        ./bin/external/yq d ${TMP_FILE} services.${SERVICE_NAME} -i
    fi
}

filter "DEFAULT_START_PHPMYADMIN" "phpmyadmin"
filter "DEFAULT_START_NGINX" "nginx"
filter "DEFAULT_START_APACHE" "apache"
filter "DEFAULT_START_MYSQL" "mysql"
filter "DEFAULT_START_MAILHOG" "mailhog"
filter "DEFAULT_START_SELENIUM_CHROME" "selenium-chrome"
filter "DEFAULT_START_SELENIUM_FIREFOX" "selenium-firefox"
filter "DEFAULT_START_HTTPBIN" "httpbin"
filter "DEFAULT_START_TEST_PICTURE_PROVIDER" "test-picture-provider"

cat ${TMP_FILE}
rm ${TMP_FILE}
cd - > /dev/null
