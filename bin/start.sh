#!/usr/bin/env bash
RUNTIME_START=$(date +%s)

DIR="$(dirname ${0})/../"
cd ${DIR}

./bin/subscripts/check_env.sh
if [[ $? -ne 0 ]]; then exit 1; fi

check() {
    SERVICES=${1}
    CONFIG_KEY=${2}
    SERVICE_NAME=${3}

    if [[ ! $(grep -r ${CONFIG_KEY} .env | cut -d= -f2) == "true" ]]; then
        SERVICES=$(echo -n ${SERVICES} | sed "s|${SERVICE_NAME}||")
    fi

    echo -n ${SERVICES}
}

echo "Check APP_DIR"
APP_DIR=$(grep -r "APP_DIR=" .env | cut -d= -f2)
ls ${APP_DIR} > /dev/null 2>&1
APP_DIR_CHECK=${?}

if [[ ${APP_DIR_CHECK} -ne 0 ]]; then
    echo "APP_DIR not found: creating directory ..."
    mkdir -p ${APP_DIR}
fi

echo "Check DATA_DIR"
DATA_DIR=$(grep -r "DATA_DIR=" .env | cut -d= -f2)
ls ${DATA_DIR} > /dev/null 2>&1
DATA_DIR_CHECK=${?}

if [[ ${DATA_DIR_CHECK} -ne 0 ]]; then
    echo "DATA_DIR not found: creating directory ..."
    mkdir -p ${DATA_DIR}
fi

set -e

if [[ -z ${1} ]]; then
    SERVICES=$(docker-compose ps --services | paste -sd " ")

    SERVICES=$(check "${SERVICES}" "DEFAULT_START_PHPMYADMIN" "phpmyadmin")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_NGINX" "nginx")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_APACHE" "apache")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_MYSQL" "mysql")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_MAILHOG" "mailhog")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_SELENIUM_CHROME" "selenium-chrome")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_SELENIUM_FIREFOX" "selenium-firefox")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_HTTPBIN" "httpbin")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_TEST_PICTURE_PROVIDER" "test-picture-provider")
else
    SERVICES=${@}
fi

echo "Starting the following services: ${SERVICES}"

./bin/subscripts/docker-compose.sh up -d ${SERVICES}
cd - > /dev/null

printf "INFO: script ${0} runtime: $(($(date +%s)-RUNTIME_START)) second(s)\n"
