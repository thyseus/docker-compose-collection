#!/usr/bin/env bash
RUNTIME_START=$(date +%s)

DIR=$(dirname ${0})
cd ${DIR}

./subscripts/check_env.sh
if [[ $? -ne 0 ]]; then exit 1; fi

./stop.sh
./start.sh ${@}
cd - > /dev/null

printf "INFO: script ${0} runtime: $(($(date +%s)-RUNTIME_START)) second(s)\n"
