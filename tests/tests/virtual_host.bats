#!/usr/bin/env bats

load helpers

run_script () {
    run ./bin/subscripts/docker-compose.sh config
    [ $status -eq 0 ]
}

@test $(get_description "check if virtual host is set correctly") {
    run_script
    [ $(echo $output | grep -i "virtual_host" | wc -l) -eq 0 ]

    sed -i "s|^\(VIRTUAL_HOST_NGINX=\s*\).*$|\1hostnginx|" .env
    run_script
    [ $(echo $output | grep -i "VIRTUAL_HOST: hostnginx" | wc -l) -eq 1 ]

    sed -i "s|^\(VIRTUAL_HOST_APACHE=\s*\).*$|\1hostapache|" .env
    run_script
    [ $(echo $output | grep -i "VIRTUAL_HOST: hostapache" | wc -l) -eq 1 ]

    sed -i "s|^\(VIRTUAL_HOST_MAILHOG=\s*\).*$|\1hostmailhog|" .env
    run_script
    [ $(echo $output | grep -i "VIRTUAL_HOST: hostmailhog" | wc -l) -eq 1 ]

    sed -i "s|^\(VIRTUAL_HOST_PHPMYADMIN=\s*\).*$|\1hostphpmyadmin|" .env
    run_script
    [ $(echo $output | grep -i "VIRTUAL_HOST: hostphpmyadmin" | wc -l) -eq 1 ]

    run ./bin/subscripts/docker-compose.sh ps
    [ $status -eq 0 ]
}

@test $(get_description "check if letsencrypt env vars are set correctly") {
    run_script
    [ $(echo $output | grep -i "virtual_host" | wc -l) -eq 0 ]
    [ $(echo $output | grep -i "letsencrypt" | wc -l) -eq 0 ]

    sed -i "s|^\(LETSENCRYPT_EMAIL=\s*\).*$|\1a@b.de|" .env
    sed -i "s|^\(VIRTUAL_HOST_NGINX=\s*\).*$|\1hostnginx|" .env
    sed -i "s|^\(VIRTUAL_HOST_APACHE=\s*\).*$|\1hostapache|" .env
    sed -i "s|^\(VIRTUAL_HOST_MAILHOG=\s*\).*$|\1hostmailhog|" .env
    sed -i "s|^\(VIRTUAL_HOST_PHPMYADMIN=\s*\).*$|\1hostphpmyadmin|" .env
    run_script
    [ $(echo $output | grep -i "LETSENCRYPT_EMAIL: a@b.de" | wc -l) -eq 1 ]
    [ $(echo $output | grep -i "LETSENCRYPT_HOST: hostnginx" | wc -l) -eq 1 ]
    [ $(echo $output | grep -i "LETSENCRYPT_HOST: hostapache" | wc -l) -eq 1 ]
    [ $(echo $output | grep -i "LETSENCRYPT_HOST: hostmailhog" | wc -l) -eq 1 ]
    [ $(echo $output | grep -i "LETSENCRYPT_HOST: hostphpmyadmin" | wc -l) -eq 1 ]

    run ./bin/subscripts/docker-compose.sh ps
    [ $status -eq 0 ]
}

@test $(get_description "check if frontend network is set") {
    run_script
    [ $(echo $output | grep -i " frontend: null" | wc -l) -eq 0 ]

    sed -i "s|^\(FRONTEND_NETWORK_NAME=\s*\).*$|\1testnetwork|" .env
    run_script
    [ $(echo $output | grep -i "name: testnetwork" | wc -l) -eq 1 ]
    [ $(echo $output | grep -i " frontend: null" | wc -l) -eq 1 ]

    run ./bin/subscripts/docker-compose.sh ps
    [ $status -eq 0 ]
}
