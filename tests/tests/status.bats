#!/usr/bin/env bats

load helpers

run_script () {
    run bin/status.sh
}

run_cli_script () {
    run bin/dcc status
}

@test $(get_description "check runtime is shown") {
    run_script
    check_runtime "/status.sh"
}

@test $(get_description "check runtime is shown via cli") {
    run_cli_script
    check_runtime "/status.sh"
}

@test $(get_description "check docker-compose ps is printed") {
    EXPECTED="$(docker-compose ps)"
    run_script
    [ $(echo $output | grep "$(echo $EXPECTED)" | wc -l) -eq 1 ]
}

@test $(get_description "check if script fails when .env is missing") {
    check_env_test
}

