#!/usr/bin/env bats

load helpers

run_script () {
    run bin/dcc ${@}
}

@test $(get_description "check help text when command is empty") {
    run_script
    [ $status -eq 1 ]
    [ $(echo $output | grep -i "ERROR: no command given" | wc -l) -eq 1 ]
    [ $(echo $output | grep -i "bash" | wc -l) -eq 1 ]
    [ $(echo $output | grep -i "build" | wc -l) -eq 1 ]
    [ $(echo $output | grep -i "exec" | wc -l) -eq 1 ]
    [ $(echo $output | grep -i "init" | wc -l) -eq 1 ]
    [ $(echo $output | grep -i "restart" | wc -l) -eq 1 ]
    [ $(echo $output | grep -i "start" | wc -l) -eq 1 ]
    [ $(echo $output | grep -i "status" | wc -l) -eq 1 ]
    [ $(echo $output | grep -i "stop" | wc -l) -eq 1 ]
}

@test $(get_description "check help text when command does not exist") {
    run_script abc abc
    [ $status -eq 1 ]
    [ $(echo $output | grep -i "ERROR: command \"abc\" does not exist" | wc -l) -eq 1 ]
}

