#!/usr/bin/env bats

load helpers

@test $(get_description "check if composer cache is working") {
    ./bin/start.sh php-fpm
    ./bin/exec.sh rm -rf ~/.composer
    ./bin/exec.sh composer init -n
    run ./bin/exec.sh composer require rivsen/hello-world
    [ $(echo $output | grep -i "loading from cache" | wc -l) -eq 0 ]
    ./bin/exec.sh rm -r vendor/
    run ./bin/exec.sh composer install
    [ $(echo $output | grep -i "loading from cache" | wc -l) -eq 1 ]
}

@test $(get_description "check if cron is running") {
    ./bin/start.sh php-fpm
    run ./bin/exec.sh service cron status
    [ $(echo $output | grep -i "ok" | grep -i "running" | wc -l) -eq 1 ]
}

@test $(get_description "check if cron-scripts are copied") {
    echo "hello world 12345" > ./contexts/php-fpm/system-cron/test_cron
    ./bin/build.sh php-fpm
    ./bin/start.sh php-fpm
    COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
    run docker exec -t --user=root "${COMPOSE_PROJECT_NAME}_php-fpm_1" cat /etc/cron.d/test_cron
    [ $status -eq 0 ]
    [ $(echo $output | grep -i "hello world 12345" | wc -l) -eq 1 ]
}

@test $(get_description "check if memory limit is set correctly") {
    sed -i "s|^\(PHP_MEMORY_LIMIT=\s*\).*$|\1167|" .env
    sed -i "s|^\(PHP_UPLOAD_MAX_FILESIZE=\s*\).*$|\1168|" .env
    ./bin/build.sh php-fpm
    ./bin/start.sh php-fpm
    COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
    run docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm_1" php -r "echo ini_get('memory_limit');"
    [ $(echo $output | grep -i "167M" | wc -l) -eq 1 ]
    run docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm_1" php -r "echo ini_get('upload_max_filesize');"
    [ $(echo $output | grep -i "168M" | wc -l) -eq 1 ]
    run docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm_1" php -r "echo ini_get('post_max_size');"
    [ $(echo $output | grep -i "168M" | wc -l) -eq 1 ]
}

@test $(get_description "check xdebug extension") {
    COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
    ./bin/start.sh php-fpm
    run docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm_1" php -v
    [ $(echo $output | grep -i "xdebug" | wc -l) -eq 0 ]
    sed -i "s|^\(PHP_EXT_XDEBUG=\s*\).*$|\1true|" .env
    ./bin/build.sh php-fpm
    ./bin/restart.sh php-fpm
    run docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm_1" php -v
    [ $(echo $output | grep -i "xdebug" | wc -l) -eq 1 ]
}

@test $(get_description "check if the latest stable composer version is installed") {
    COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
    ./bin/start.sh php-fpm
    run docker exec -t -u www-data "${COMPOSE_PROJECT_NAME}_php-fpm_1" composer --version
    COMPOSER_VERSION_INSTALLED=$(echo $output | awk 'match($0,/([0-9]+\.[0-9]+\.[0-9]+)/) {print substr($0,RSTART,RLENGTH)}')
    COMPOSER_TAGS=$(docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm_1" git ls-remote --tags https://github.com/composer/composer.git)
    COMPOSER_LAST_STABLE_VERSION=$(echo "$COMPOSER_TAGS" | awk -F'/' '/[0-9]+\.[0-9]+\.[0-9]+[\r]$/ { print $3}' | tail -1 | tr -d '\r')
    [ "$COMPOSER_VERSION_INSTALLED" = "$COMPOSER_LAST_STABLE_VERSION" ]
}

@test $(get_description "check if sendmail for mailhog correctly installed and configured") {
    sed -i "s|^\(MAILHOG_SENDMAIL=\s*\).*$|\1true|" .env
    ./bin/build.sh php-fpm
    ./bin/start.sh php-fpm
    check_package_is_installed "ssmtp"
    [ $status -eq 0 ]
    check_package_is_installed "mailutils"
    [ $status -eq 0 ]
    COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
    MAILHOG_SMTP_PORT=$(grep -r MAILHOG_SMTP_PORT .env | cut -d= -f2)
    run docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm_1" php -r "echo ini_get('sendmail_path');"
    [ $(echo $output | grep -i "/usr/sbin/sendmail -S mailhog:${MAILHOG_SMTP_PORT}" | wc -l) -eq 1 ]
    run docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm_1" cat /etc/ssmtp/ssmtp.conf
    [ $(echo $output | grep -i "mailhub=mailhog:${MAILHOG_SMTP_PORT}" | wc -l) -eq 1 ]
}

@test $(get_description "check mysqli extension") {
    sed -i "s|^\(PHP_EXT_MYSQLI=\s*\).*$|\1true|" .env
    ./bin/build.sh php-fpm
    ./bin/start.sh php-fpm
    check_extension 'mysqli'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 1 ]
}

@test $(get_description "check mbstring extension") {
    ./bin/build.sh php-fpm
    ./bin/start.sh php-fpm
    check_extension 'mbstring'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 1 ]
}

@test $(get_description "check pdo mysql extension") {
    sed -i "s|^\(PHP_EXT_PDO_MYSQL=\s*\).*$|\1true|" .env
    ./bin/build.sh php-fpm
    ./bin/start.sh php-fpm
    check_extension 'pdo_mysql'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 1 ]
}

@test $(get_description "check zip extension") {
    sed -i "s|^\(PHP_EXT_ZIP=\s*\).*$|\1true|" .env
    ./bin/build.sh php-fpm
    ./bin/start.sh php-fpm
    check_extension 'zip'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 1 ]
}

@test $(get_description "check ldap extension") {
    sed -i "s|^\(PHP_EXT_LDAP=\s*\).*$|\1true|" .env
    ./bin/build.sh php-fpm
    ./bin/start.sh php-fpm
    check_extension 'ldap'
    [ $status -eq 0 ]
    [ $(echo $output) -eq 1 ]
}