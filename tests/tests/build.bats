#!/usr/bin/env bats

load helpers

run_script () {
    run bin/build.sh nginx
}

run_cli_script () {
    run bin/dcc build nginx
}

@test $(get_description "check runtime is shown") {
    run_script
    check_runtime "/build.sh"
}

@test $(get_description "check runtime is shown via cli") {
    run_cli_script
    check_runtime "/build.sh"
}

@test $(get_description "check if script fails when .env is missing") {
    check_env_test
}

@test $(get_description "check that old images are deleted") {
    bin/build.sh mailhog
    [ $(docker images | grep -i "<none>" | wc -l) -eq 0 ]
    [ $(docker images | grep -i "mailhog" | wc -l) -eq 2 ]
    bin/build.sh mailhog
    [ $(docker images | grep -i "<none>" | wc -l) -eq 0 ]
}

@test $(get_description "check that only images from this project are deleted") {
    docker-compose build --no-cache mailhog
    bin/build.sh mailhog
    [ $(docker images | grep -i "<none>" | wc -l) -eq 1 ]
    docker rmi $(docker images -q)
}