#!/usr/bin/env bats

load helpers

run_script () {
    run bin/start.sh
}

run_cli_script () {
    run bin/dcc start
}

@test $(get_description "check runtime is shown") {
    run_script
    check_runtime "/start.sh"
}

@test $(get_description "check runtime is shown via cli") {
    run_cli_script
    check_runtime "/start.sh"
}

@test $(get_description "check if script fails when .env is missing") {
    check_env_test
}

