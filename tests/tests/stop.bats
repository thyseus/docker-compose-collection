#!/usr/bin/env bats

load helpers

run_script () {
    run bin/stop.sh
}

run_cli_script () {
    run bin/dcc stop
}

@test $(get_description "check runtime is shown") {
    run_script
    check_runtime "/stop.sh"
}

@test $(get_description "check runtime is shown via cli") {
    run_cli_script
    check_runtime "/stop.sh"
}

@test $(get_description "check if script fails when .env is missing") {
    check_env_test
}

