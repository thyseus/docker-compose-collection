#!/usr/bin/env bats

load helpers

run_script () {
    run bin/exec.sh date
}

run_cli_script () {
    run bin/dcc exec date
}

@test $(get_description "check runtime is shown") {
    run_script
    check_runtime "/exec.sh"
}

@test $(get_description "check runtime is shown via cli") {
    run_cli_script
    check_runtime "/exec.sh"
}

@test $(get_description "check if script fails when .env is missing") {
    check_env_test
}

